<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210320163755 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE score (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, serie_id INTEGER NOT NULL, player_id INTEGER NOT NULL, date DATE NOT NULL, point INTEGER NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX IDX_32993751D94388BD ON score (serie_id)');
        $this->addSql('CREATE INDEX IDX_3299375199E6F5DF ON score (player_id)');
        $this->addSql('CREATE TABLE serie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, creator_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, difficulty VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX IDX_AA3A933461220EA6 ON serie (creator_id)');
        $this->addSql('CREATE TABLE "user" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE TABLE word (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, word VARCHAR(255) NOT NULL, image_file_name VARCHAR(255) DEFAULT NULL, sound_file_name VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL)');
        $this->addSql('CREATE TABLE word_serie (word_id INTEGER NOT NULL, serie_id INTEGER NOT NULL, PRIMARY KEY(word_id, serie_id))');
        $this->addSql('CREATE INDEX IDX_748998F2E357438D ON word_serie (word_id)');
        $this->addSql('CREATE INDEX IDX_748998F2D94388BD ON word_serie (serie_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE score');
        $this->addSql('DROP TABLE serie');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE word');
        $this->addSql('DROP TABLE word_serie');
    }
}
