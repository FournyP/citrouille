<?php

namespace App\DataFixtures;

use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Repository\UserRepository;
use App\Repository\SerieRepository;
use App\Entity\Score;

class ScoreFixtures extends Fixture implements DependentFixtureInterface
{
    private UserRepository $userRepository;
    
    private SerieRepository $serieRepository;

    public function __construct(UserRepository $userRepository, SerieRepository $serieRepository)
    {
        $this->userRepository = $userRepository;
        $this->serieRepository = $serieRepository;
    }

    public function load(ObjectManager $manager)
    {
        $users = $this->userRepository->findAll();
        $series = $this->serieRepository->findAll();

        $faker = Factory::create();

        for ($index = 0; $index < 5; $index ++)
        {
            $serie = $series[$faker->numberBetween(max: count($series) - 1)];
            $user = $users[$faker->numberBetween(max: count($users) - 1)];

            $score = new Score();
            $score->setDate(new \DateTime('now'))
                  ->setPlayer($user)
                  ->setSerie($serie)
                  ->setPoint($faker->numberBetween(max: count($serie->getWords()) - 1));

            $manager->persist($score);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            SerieFixtures::class,
            WordFixtures::class
        ];
    }
}
