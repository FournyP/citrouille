<?php

namespace App\DataFixtures;

use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Repository\SerieRepository;
use App\Entity\Word;

class WordFixtures extends Fixture implements DependentFixtureInterface
{
    private SerieRepository $serieRepository;

    public function __construct(SerieRepository $serieRepository)
    {
        $this->serieRepository = $serieRepository;
    }

    public function load(ObjectManager $manager)
    {
        $series = $this->serieRepository->findAll();
        $words = [
            'alpaga',
            'chat',
            'chien',
            'fennec',
            'geosesarma',
            'gorille',
            'kangourou',
            'kiwi',
            'koala',
            'marmotte',
            'oie',
            'ours',
            'paresseux',
            'phoque',
            'quokka',
            'tarsier'
        ];

        for ($indexSeries = 0; $indexSeries < count($series); $indexSeries++) {
            foreach ($words as $mot) {

                $files = \glob('./public/uploads/images/' . $mot . '.*');
                $fileName = $files[0];

                $word = new Word();
                $word->setWord($mot)
                    ->addSeries($series[$indexSeries])
                    ->setImageFileName(basename($fileName));

                $manager->persist($word);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SerieFixtures::class
        ];
    }
}
