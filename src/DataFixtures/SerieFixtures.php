<?php

namespace App\DataFixtures;

use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Repository\UserRepository;
use App\Entity\Serie;

class SerieFixtures extends Fixture implements DependentFixtureInterface
{
    private const DIFFICULTIES = [
        "easy",
        "medium",
        "hard"
    ];

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function load(ObjectManager $manager)
    {
        $users = $this->userRepository->findAll();

        $faker = Factory::create();

        for ($i = 0; $i < 4; $i++)
        {
            $difficulty = self::DIFFICULTIES[$faker->numberBetween(max: count(self::DIFFICULTIES) - 1)];
            $user = $users[$faker->numberBetween(max: count($users) - 1)];

            $serie = new Serie();
            $serie->setName($faker->words(3, true))
                  ->setDifficulty($difficulty)
                  ->setCreator($user);

            $manager->persist($serie);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
