<?php

namespace App\Entity;

use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=WordRepository::class)
 * @Vich\Uploadable
 */
class Word
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("word:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("word:read")
     */
    private $word;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("word:read")
     */
    private $imageFileName;

    /**
     * @Vich\UploadableField(mapping="word_img", fileNameProperty="imageFileName")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("word:read")
     */
    private $soundFileName;

    /**
     * @Vich\UploadableField(mapping="word_soud", fileNameProperty="soundFileName")
     */
    private $soundFile;

    /**
     * @ORM\ManyToMany(targetEntity=Serie::class, inversedBy="words")
     */
    private $series;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("word:read")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("word:read")
     */
    private $createdAt;

    public function __construct()
    {
        $this->series = new ArrayCollection();
        $this->updatedAt = new \DateTime();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function setImageFileName(?string $imageFileName): self
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $imageFile): self
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getSoundFileName(): ?string
    {
        return $this->soundFileName;
    }

    public function setSoundFileName(?string $soundFileName): self
    {
        $this->soundFileName = $soundFileName;

        return $this;
    }

    public function getSoundFile(): ?File
    {
        return $this->soundFile;
    }

    public function setSoundFile(?File $soundFile): self
    {
        $this->soundFile = $soundFile;

        if (null !== $soundFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return Collection|Serie[]
     */
    public function getSeries(): Collection
    {
        return $this->series;
    }

    public function addSeries(Serie $series): self
    {
        if (!$this->series->contains($series)) {
            $this->series[] = $series;
        }

        return $this;
    }

    public function removeSeries(Serie $series): self
    {
        $this->series->removeElement($series);

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
