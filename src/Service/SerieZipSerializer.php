<?php

namespace App\Service;

use ZipArchive;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\HttpFoundation\File\File;
use App\Entity\Word;
use App\Entity\Serie;

class SerieZipSerializer
{
    private const ZIP_FILENAME = "Dictee.zip";

    /**
     * @var CsvEncoder
     */
    private CsvEncoder $encoder;

    public function __construct()
    {
        $this->encoder = new CsvEncoder([
            'csv_headers' => ['name', 'difficulty', 'words'],
            'csv_delimiter' => ";"
        ]);
    }

    /**
     * Function to serialize Serie into zip
     *
     * @param Serie $serieToSerialize
     * @return \ZipArchive
     */
    public function serialize(Serie $serieToSerialize): \ZipArchive
    {
        //Create the file
        $file = \fopen(self::ZIP_FILENAME, 'a+');
        \fclose($file);

        $zip = new \ZipArchive();
        $zip->open(self::ZIP_FILENAME, \ZipArchive::OVERWRITE);

        $serieDatas = $this->transformSerieToArray($serieToSerialize);

        $csvContent = $this->encoder->encode($serieDatas, 'csv');

        $zip->addFromString('words.csv', $csvContent);

        foreach ($serieToSerialize->getWords() as $word) {
            if ($word->getImageFileName()) {
                $zip->addFile('../public/uploads/images/' . $word->getImageFileName(), $word->getImageFileName());
            }

            if ($word->getSoundFileName()) {
                $zip->addFile('../public/uploads/sounds/' . $word->getSoundFileName(), $word->getSoundFileName());
            }
        }

        return $zip;
    }

    /**
     * Function to deserialize zip into serie
     *
     * @param ZipArchive $zipToDeserialize
     * @return Serie
     */
    public function deserialize(\ZipArchive $zipToDeserialize): Serie
    {
        $csvContent = $zipToDeserialize->getFromName('words.csv');

        $data = $this->encoder->decode($csvContent, 'csv');

        return $this->transformArrayToSerie($data[0], $zipToDeserialize);
    }

    /**
     * Function to transform a serie into an array to be serialized into csv
     *
     * @param Serie $serie
     * @return array
     */
    private function transformSerieToArray(Serie $serie): array
    {
        $serieDatas = [
            'name' => $serie->getName(),
            'difficulty' => $serie->getDifficulty()
        ];

        $wordsData = [];

        foreach ($serie->getWords() as $word) {
            $wordsData[] = \implode(',', [
                'name' => $word->getWord(),
                'imageFilename' => $word->getImageFileName(),
                'soundFilename' => $word->getSoundFileName()
            ]);
        }

        $serieDatas['words'] = \implode('|', $wordsData);

        return $serieDatas;
    }

    /**
     * Function to transform an array into a serie
     *
     * @param array $data
     * @return Serie
     */
    private function transformArrayToSerie(array $data, \ZipArchive $zip): Serie
    {
        $serie = new Serie();

        if (isset($data['name'])) {
            $serie->setName($data['name']);
        }

        if (isset($data['difficulty'])) {
            $serie->setDifficulty($data['difficulty']);
        }

        $wordDatas = \explode('|', $data['words']);

        foreach ($wordDatas as $wordData) {

            $wordData = \explode(',', $wordData);
            
            $word = new Word();
            
            if (isset($wordData[0]) && $wordData[0]) {
                $word->setWord($wordData[0]);
            }

            if (isset($wordData[1]) && $wordData[1]) {
                $file = $this->extractFileFromZip($zip, $wordData[1], '../public/uploads/images/');
                $word->setImageFileName($file->getFilename())
                    ->setImageFile($file);
            }

            if (isset($wordData[2]) && $wordData[2]) {
                $file = $this->extractFileFromZip($zip, $wordData[2], '../public/uploads/sounds/');
                $word->setSoundFileName($file->getFilename())
                    ->setSoundFile($file);
            }

            $serie->addWord($word);
        }

        return $serie;
    }

    /**
     * Function to extract a file from a zpi archive
     *
     * @param \ZipArchive $zip
     * @param string $filenameToExtract
     * @param string $dir
     * @return File
     */
    private function extractFileFromZip(\ZipArchive $zip, string $filenameToExtract, string $dir): File
    {
        $fileContent = $zip->getFromName($filenameToExtract);

        $url = $dir . \str_replace('.', '', \uniqid('', true));
        \file_put_contents($url, $fileContent);

        return new File($url);
    }
}
