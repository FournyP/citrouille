<?php

namespace App\Repository;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Entity\Word;

/**
 * @method Word|null find($id, $lockMode = null, $lockVersion = null)
 * @method Word|null findOneBy(array $criteria, array $orderBy = null)
 * @method Word[]    findAll()
 * @method Word[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Word::class);
    }

    /**
     * @return Word[]
     */
    public function findByFilters(array $filters): array
    {
        $query = $this->createQueryBuilder('w');

        if (isset($filters["word"])) {
            $query->andWhere("w.word LIKE :word")
                ->setParameter('word', "%" . $filters["word"] . "%");
        }

        if (isset($filters["imageFilename"])) {
            $query->andWhere("w.imageFilename LIKE :imageFilename")
                ->setParameter('imageFilename', "%" . $filters["imageFilename"] . "%");
        }

        if (isset($filters["soundFilename"])) {
            $query->andWhere("w.soundFilename LIKE :soundFilename")
                ->setParameter('soundFilename', "%" . $filters["soundFilename"] . "%");
        }

        return $query->getQuery()->getResult();
    }
}
