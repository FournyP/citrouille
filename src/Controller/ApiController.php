<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\WordRepository;

class ApiController extends AbstractController
{
    #[Route('/api/words', name: 'api_words', methods: ["GET"])]
    #[IsGranted('ROLE_PROF')]
    public function getWords(WordRepository $wordRepository, Request $request): Response
    {
        $words = $wordRepository->findByFilters($request->query->all());

        return $this->json($words, 200, [], ['groups' => 'word:read']);
    }
}
