<?php

namespace App\Controller;

use ZipArchive;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\SerieZipSerializer;
use App\Repository\SerieRepository;
use App\Form\SerieUploadType;
use App\Form\SerieType;
use App\Entity\Serie;
use App\Entity\Score;

class SerieController extends AbstractController
{
    private const ERROR_TEMPLATE_NAME = "error.html.twig";

    #[Route('/serie/new', name: 'serie_new')]
    #[Route('/serie/edit/{id}', name: 'serie_edit')]
    #[IsGranted('ROLE_PROF')]
    public function form(Request $request, EntityManagerInterface $manager, Serie $serie = null)
    {
        $editMode = true;

        if ($serie === null) {
            $serie = new Serie();
            $editMode = false;
        }

        $form = $this->createForm(SerieType::class, $serie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($serie);
            $serie->setCreator($this->getUser());

            foreach ($serie->getWords() as $word) {
                $manager->persist($word);
            }

            $manager->flush();

            return $this->redirectToRoute('series_table');
        }

        return $this->render('serie/form.html.twig', [
            'form' => $form->createView(),
            'edit_mode' => $editMode
        ]);
    }

    #[Route('/series', name: 'series_table')]
    public function tableSeries(SerieRepository $serieRepository): Response
    {
        return $this->render('serie/table.html.twig', [
            'series' => $serieRepository->findAll()
        ]);
    }

    #[Route('/game/{id}', name: 'game', methods:['GET'])]
    public function game(Serie $serie = null): Response
    {
        if ($serie == null) {
            return $this->render(self::ERROR_TEMPLATE_NAME);
        }
        
        return $this->render('serie/game.html.twig', [
            'serie' => $serie,
            'id' => $serie->getId()
        ]);
    }

    #[Route('/game/{id}/result', name: 'result', methods: ['POST'])]
    public function result(Request $request, EntityManagerInterface $manager, Serie $serie = null): Response
    {
        if ($serie == null) {
            return $this->render(self::ERROR_TEMPLATE_NAME);
        }

        $words = [];
        $points = 0;

        foreach ($serie->getWords() as $word) {
            $words[$word->getWord()] = '';
        }

        foreach ($request->request->all() as $key => $param) {
            if ($param == $key) {
                $points++;
            }

            if (isset($words[$key])) {
                $words[$key] = $param;
            }
        }

        if ($this->getUser() != null) {
            $score = new Score();
            $score
                ->setPlayer($this->getUser())
                ->setSerie($serie)
                ->setPoint($points)
                ->setDate(new \DateTime());

            $manager->persist($score);
            $manager->flush();
        }

        return $this->render('serie/result.html.twig', [
            'results' => $words,
            'points' => $points,
            'maxPoints' => \count($words)
        ]);
    }

    #[Route('/download/serie/{id}', name: 'download_serie')]
    public function download(SerieZipSerializer $zipSerializer, Serie $serie = null): Response
    {
        if ($serie == null) {
            return $this->render(self::ERROR_TEMPLATE_NAME);
        }

        $zip = $zipSerializer->serialize($serie);
        $filename = $zip->filename;
        $zip->close();
        
        $newFilename = \sys_get_temp_dir() . \basename($filename);

        \rename($filename, $newFilename);

        return $this->file($newFilename);
    }

    #[Route('/upload/serie', name: 'upload_serie')]
    #[IsGranted('ROLE_PROF')]
    public function upload(SerieZipSerializer $zipSerializer, Request $request, EntityManagerInterface $manager, ValidatorInterface $validator): Response
    {
        $form = $this->createForm(SerieUploadType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $uploadedFile = $form->getData()['zipFile'];

            $zip = new ZipArchive();
            $zip->open($uploadedFile->getPathname());

            $serie = $zipSerializer->deserialize($zip);
            $serie->setCreator($this->getUser());

            $errors = $validator->validate($serie);

            foreach($errors as $error) {
                $form->addError($error->getMessage());
            }

            if ($form->isValid()) {

                $manager->persist($serie);

                foreach ($serie->getWords() as $word) {
                    $manager->persist($word);
                }

                $manager->flush();

                return $this->redirectToRoute('series_table');
            }
        }

        return $this->render("serie/form_upload.html.twig", [
            'form' => $form->createView()
        ]);
    }
}
