# About 

A simple project realized with ESIEE-IT

# Author

Morisse Vincent, Fourny Pierre

# Dependencies

- php 8
- npm or yarn

# How to install ?

```
git clone https://gitlab.com/FournyP/citrouille.git
cd citrouille
composer install
npm run build
php bin/console doctine:migrations:migrate
php bin/console doctrine:fixtures:load
```

# How to run ?

After install all dependencies, run the command :
`symfony server:start -d` and go to 127.0.0.1:8000 in your web browser

Or :
`php -S 127.0.0.1:8000` and go to 127.0.0.1:8000/public/index.php in your web browser

# Youtube presentation link

https://youtu.be/wQ-0EC1UEyk
