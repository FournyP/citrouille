require('jquery-ui/ui/widgets/autocomplete');
var dynamicItemsUtils = require('./utils/dynamic-items-form');

$(document).ready(function () {
    // get the collectionHolder, initialize the var by getting the list;
    let collectionHolder = $('#wordList');

    let addNewItem = $('#btn-add-word');

    // add an index property to the collectionHolder which helps track the count of forms we have in the list
    collectionHolder.data('index', collectionHolder.find('.card').length)

    collectionHolder.find('.card').each(function () {
        dynamicItemsUtils.addRemoveButton($(this), collectionHolder);
    });

    // handle the click event for addNewItem
    addNewItem.on('click', function (e) {
        e.preventDefault();
        dynamicItemsUtils.addNewForm(collectionHolder, $(this));
    });
});
