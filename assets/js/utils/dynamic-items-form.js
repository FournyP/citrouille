function addNewForm(collectionHolder, position) {
    
    let prototype = collectionHolder.data('prototype');
    let index = collectionHolder.data('index');

    prototype = prototype.replace(/__name__/g, index);
    collectionHolder.data('index', index + 1);

    // this is the card that will be appending to the collectionHolder
    let card = $('<div class="card" id="' + index + '"><div class="card-header">N°' + (index + 1) + '</div></div>');
    let cardBody = $('<div class="card-body"></div>').append(prototype);

    card.append(cardBody);
    addRemoveButton(card, collectionHolder);

    position.before(card);

    //For autocomplete of word
    cardBody.children(".row").children(".col").children(".form-group").children(".word-autocomplete").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "/api/words",
                type: "GET",
                dataType: "json",
                data: 'word=' + request.term,
                success: function (result) {
                    
                    let mappedResult = result.map(word => word.word);

                    response(mappedResult);
                }
            });
        }
    });

    cardBody.children('.row').children('.col').children('.form-group').children('.custom-file').children('.custom-file-input').on('change', function() {
        let filename = $(this)[0].files[0].name;
        $(this).parent('.custom-file').children('.custom-file-label').text(filename);
    });
}

function addRemoveButton(card, collectionHolder) {
    let removeButton = $('<a href="#" class="btn btn-danger">Supprimer</a>');
    // appending the removeButton to the panel footer
    let cardFooter = $('<div class="card-footer"></div>').append(removeButton);

    // handle the click event of the remove button
    removeButton.on('click', function (e) {
        e.preventDefault();
        $(e.target).parents('.card').slideUp(1000, function () {
            $(this).remove();
            updateAllCard();
        })
        let index = collectionHolder.data('index');
        collectionHolder.data('index', index - 1);
    });

    card.append(cardFooter);
}

function updateAllCard() {
    $('.card-header').each(function (index) {
        $(this).text('N°' + (index + 1));
        $(this).parents('.card')[0].id = index;
    })
}

module.exports = {addNewForm, addRemoveButton};