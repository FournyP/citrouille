$(document).ready(function () {
  $(".carousel-item")[0].classList.add("active");

  $("input[type=text]").on("keypress", function (e) {
    if (e.keyCode === 13) {
        e.preventDefault();
        $("#carouselGame").carousel("next");
    }
  });
});
